<?php
session_start(); 
include("config/fonction.php");
connect();
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Shopping</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>Authentification</h1>
                        
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>connecter a votre espace</h3>
                            		<p>Enter votre login et mot de passe:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">


			                    <form method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username" >Login</label>
			                        	<input type="text" name="login" placeholder="Identifiant..." class="form-username form-control" id="form-username" required>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Mot de passe</label>
			                        	<input type="password" name="password" placeholder="Mot de passe..." class="form-password form-control" id="form-password" required>
			                        </div>
			                        <input type="submit" class="btn btn-lg btn-primary bbtn"  name="submit" value="connexion"/>
			                    </form>

                                <?php

                                if (isset($_POST["submit"]))
								 {
										 if (compteurTable("admin","where login='".$_POST['login']."' and mdp='".$_POST['password']."'")>0) 
										 {
											 $_SESSION['log']=$_POST['login'];
											 header("location:admin/index.php") ;                                     
	
										 }else
										 {
											 echo "<script>alert('Login ou mot de passe sont incorrect');</script>";
										 }
                                 } 
                                 ?>


		                    </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
       
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
      
            <!-- ajax -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
         <script type="text/Javascript" src="js/recherche.js"></script>
  
    </body>

</html>