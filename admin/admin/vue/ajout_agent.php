            
<?php
if (isset($_POST['ajt'])) {
    if (compteurTable("agent", "where cin_agent='" . $_POST['cin'] . "'") > 0) {
        ?>
        <div class="alert alert-danger " role="alert" style="text-align: center;">Agent existe déjà</div>  
        <?php
    } else {
        $content_dir = 'photoAgent/'; // dossier où sera déplacé le fichier

        $tmp_file = $_FILES['photo']['tmp_name'];

        if (!is_uploaded_file($tmp_file)) {
            exit("Le fichier est introuvable");
        }


        // on copie le fichier dans le dossier de destination
        $name_file = $_FILES['photo']['name'];

        if (!move_uploaded_file($tmp_file, $content_dir . $name_file)) {
            exit("Impossible de copier le fichier dans $content_dir");
        }

        $photo = $_FILES['photo']['name'];

        $agent = new agent(NULL, $_POST['cin'], $photo, $_POST['nom'], $_POST['prenom'], $_POST['genre'], $_POST['tel'], $_POST['email'], $_POST['mdpass'], $_POST['ville'], $_POST['poste'], $_POST['dt']);
        $agent->ajouter_agent();
        ?>
        <div class="alert alert-success " role="alert" style="text-align: center;">Agent ajoutée avec sucee</div> 
        <?php
    }
}
?>
<div class="contentpanel">

    <div class="row">
        <div class="col-md-8 col-sm-offset-2 col-xs-12 ">
            <form method="post" enctype="multipart/form-data">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"  style="text-align: center">Ajouter des Agents</h4>
                    </div><!-- panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">CIN <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="cin" class="form-control" placeholder="CIN.." required="" oninvalid="this.setCustomValidity('vous avez oublier de remplire le champs reference')" />
                                </div>
                            </div><!-- form-group -->


                            <div class="form-group">
                                <label class="col-sm-3 control-label">photo <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="file" name="photo" class="form-control" />
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nom Agent <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="nom" class="form-control" />
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Prenom Agent <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="prenom" class="form-control" />
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">genre <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <select name="genre" class="form-control" required/> 
                                    <option>  </option>
                                    <option> Homme </option>
                                    <option> Femme </option>
                                    </select>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Téléphone <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="tel" placeholder="Enter le Telephone..." required/>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" class="form-control"  name="marque" required>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mot de Passe <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="mdpass" placeholder="Entre le mot de passe..." required/>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Ville <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="ville" placeholder="Entre la ville..." required/>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Code Postale <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="poste" placeholder="Entre le code postale..." required/>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Date d'embauche <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" name="dt" placeholder="Entre la dare d'embauche..." required/>
                                </div>
                            </div><!-- form-group -->

                        </div><!-- row -->
                    </div><!-- panel-body -->
                    <div class="panel-footer">
                        <span class="asterisk" style="text-align: right;">(*) champ obligatoire!</span>
                        <div class="row">

                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="submit" name="ajt" value="Ajouter" class="btn btn-primary mr5">
                                <button type="reset" class="btn btn-dark">Annuler</button>
                            </div>

                        </div>

                    </div><!-- panel-footer -->  
                </div>
                 </form>
        </div><!-- panel -->
      

    </div><!-- col-md-6 -->
</div><!-- contentpanel -->

 

