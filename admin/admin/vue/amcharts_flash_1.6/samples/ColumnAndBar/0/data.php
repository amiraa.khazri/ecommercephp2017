<?php
session_start();
echo'<?xml version="1.0" encoding="UTF-8"?>
<chart>
	<series>
		<value xid="Janvier">Janvier</value>
		<value xid="Fevrier">Fevrier</value>
		<value xid="Mars">Mars</value>
		<value xid="Avril">Avril</value>
		<value xid="Mai">Mai</value>
		<value xid="Juin">Juin</value>
		<value xid="Juillet">Juillet</value>
		<value xid="Aout">Aout</value>
		<value xid="Septembre">Septembre</value>
		<value xid="Octobre">Octobre</value>
		<value xid="Novembre">Novembre</value>
		<value xid="Decembre">Decembre</value>
	</series>
	<graphs>
		<graph gid="Imprimente" title="Imprimente">
			<value xid="Janvier">'.$_SESSION['cc01c1'].'</value>
			<value xid="Fevrier">'.$_SESSION['cc02c1'].'</value>
			<value xid="Mars">'.$_SESSION['cc03c1'].'</value>
			<value xid="Avril">'.$_SESSION['cc04c1'].'"</value>
			<value xid="Mai">'.$_SESSION['cc05c1'].'</value>
			<value xid="Juin">'.$_SESSION['cc06c1'].'</value>
			<value xid="Juillet">'.$_SESSION['cc07c1'].'</value>
			<value xid="Aout">'.$_SESSION['cc08c1'].'</value>
			<value xid="Septembre">'.$_SESSION['cc09c1'].'</value>
			<value xid="Octobre">'.$_SESSION['cc10c1'].'</value>
			<value xid="Novembre">'.$_SESSION['cc11c1'].'</value>
			<value xid="Decembre">'.$_SESSION['cc12c1'].'</value>
		</graph>
		<graph gid="Laptop" title="Laptop">
			<value xid="Janvier">'.$_SESSION['cc01c3'].'</value>
			<value xid="Fevrier">'.$_SESSION['cc02c3'].'</value>
			<value xid="Mars">'.$_SESSION['cc03c3'].'</value>
			<value xid="Avril">'.$_SESSION['cc04c3'].'"</value>
			<value xid="Mai">'.$_SESSION['cc05c3'].'</value>
			<value xid="Juin">'.$_SESSION['cc06c3'].'</value>
			<value xid="Juillet">'.$_SESSION['cc07c3'].'</value>
			<value xid="Aout">'.$_SESSION['cc08c3'].'</value>
			<value xid="Septembre">'.$_SESSION['cc09c3'].'</value>
			<value xid="Octobre">'.$_SESSION['cc10c3'].'</value>
			<value xid="Novembre">'.$_SESSION['cc11c3'].'</value>
			<value xid="Decembre">'.$_SESSION['cc12c3'].'</value>
		</graph>		
		<graph gid="Appareil Photo" title="Appareil Photo">
			<value xid="Janvier">'.$_SESSION['cc01c5'].'</value>
			<value xid="Fevrier">'.$_SESSION['cc02c5'].'</value>
			<value xid="Mars">'.$_SESSION['cc03c5'].'</value>
			<value xid="Avril">'.$_SESSION['cc04c5'].'</value>
			<value xid="Mai">'.$_SESSION['cc05c5'].'</value>
			<value xid="Juin">'.$_SESSION['cc06c5'].'</value>
			<value xid="Juillet">'.$_SESSION['cc07c5'].'</value>
			<value xid="Aout">'.$_SESSION['cc08c5'].'</value>
			<value xid="Septembre">'.$_SESSION['cc09c5'].'</value>
			<value xid="Octobre">'.$_SESSION['cc10c5'].'</value>
			<value xid="Novembre">'.$_SESSION['cc11c5'].'</value>
			<value xid="Decembre">'.$_SESSION['cc12c5'].'</value>
		</graph>		
		<graph gid="Autres Accessoires" title="Autres Accessoires">
			<value xid="Janvier">'.$_SESSION['cc01c6'].'</value>
			<value xid="Fevrier">'.$_SESSION['cc02c6'].'</value>
			<value xid="Mars">'.$_SESSION['cc03c6'].'</value>
			<value xid="Avril">'.$_SESSION['cc04c6'].'"</value>
			<value xid="Mai">'.$_SESSION['cc05c6'].'</value>
			<value xid="Juin">'.$_SESSION['cc06c6'].'</value>
			<value xid="Juillet">'.$_SESSION['cc07c6'].'</value>
			<value xid="Aout">'.$_SESSION['cc08c6'].'</value>
			<value xid="Septembre">'.$_SESSION['cc09c6'].'</value>
			<value xid="Octobre">'.$_SESSION['cc10c6'].'</value>
			<value xid="Novembre">'.$_SESSION['cc11c6'].'</value>
			<value xid="Decembre">'.$_SESSION['cc12c6'].'</value>
		</graph>		
		<graph gid="Tabllete" title="Tabllete">
			<value xid="Janvier">'.$_SESSION['cc01c2'].'</value>
			<value xid="Fevrier">'.$_SESSION['cc02c2'].'</value>
			<value xid="Mars">'.$_SESSION['cc03c2'].'</value>
			<value xid="Avril">'.$_SESSION['cc04c2'].'"</value>
			<value xid="Mai">'.$_SESSION['cc05c2'].'</value>
			<value xid="Juin">'.$_SESSION['cc06c2'].'</value>
			<value xid="Juillet">'.$_SESSION['cc07c2'].'</value>
			<value xid="Aout">'.$_SESSION['cc08c2'].'</value>
			<value xid="Septembre">'.$_SESSION['cc09c2'].'</value>
			<value xid="Octobre">'.$_SESSION['cc10c2'].'</value>
			<value xid="Novembre">'.$_SESSION['cc11c2'].'</value>
			<value xid="Decembre">'.$_SESSION['cc12c2'].'</value>
		</graph>		
		<graph gid="Telephone/GSM" title="Telephone/GSM">
			<value xid="Janvier">'.$_SESSION['cc01c4'].'</value>
			<value xid="Fevrier">'.$_SESSION['cc02c4'].'</value>
			<value xid="Mars">'.$_SESSION['cc03c4'].'</value>
			<value xid="Avril">'.$_SESSION['cc04c4'].'"</value>
			<value xid="Mai">'.$_SESSION['cc05c4'].'</value>
			<value xid="Juin">'.$_SESSION['cc06c4'].'</value>
			<value xid="Juillet">'.$_SESSION['cc07c4'].'</value>
			<value xid="Aout">'.$_SESSION['cc08c4'].'</value>
			<value xid="Septembre">'.$_SESSION['cc09c4'].'</value>
			<value xid="Octobre">'.$_SESSION['cc10c4'].'</value>
			<value xid="Novembre">'.$_SESSION['cc11c4'].'</value>
			<value xid="Decembre">'.$_SESSION['cc12c4'].'</value>
		</graph>		
	</graphs>
</chart>';
?>
