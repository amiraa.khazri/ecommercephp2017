<?php
if (isset($_POST['modif'])) {
    
        $content_dir = 'photoAgent/'; // dossier où sera déplacé le fichier

        $tmp_file = $_FILES['photo']['tmp_name'];

        if (!is_uploaded_file($tmp_file)) {
            exit("Le fichier est introuvable");
        }


        // on copie le fichier dans le dossier de destination
        $name_file = $_FILES['photo']['name'];

        if (!move_uploaded_file($tmp_file, $content_dir . $name_file)) {
            exit("Impossible de copier le fichier dans $content_dir");
        }

        $photo = $_FILES['photo']['name'];

        $clt= new client(NULL,$_POST['cin'],$photo ,$_POST['nom'],$_POST['prenom'],$_POST['genre'],$_POST['adresse'],$_POST['tel'],$_POST['email'],$_POST['pass'],$_POST['ville'],$_POST['poste'],$_POST['dtn']);
            $id=$_GET["modif_clt"];
            $clt->modifier_client($id);
        ?>
        <div class="alert alert-success " role="alert" style="text-align: center;">Client Modifier avec sucee</div> 
        <?php
    }
    $i=0;
    $c=new client();
    $clause="where id_client='".$_GET['modif_clt']."'";
    while($i<compteurtable("client",$clause))
    {
            $c->affiche_client($i,$clause);

    ?>

         <div class="contentpanel">
                        
                        <div class="row">
                            <div class="col-md-8 col-sm-offset-2 col-xs-12 ">
                                <form method="post" enctype="multipart/form-data">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title" style="text-align: center">Modifier Client</h4>
                                    </div><!-- panel-heading -->
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">CIN <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="cin" class="form-control" value="<?php echo $c->cin_client ; ?>" required />
                                                </div>
                                            </div><!-- form-group -->
                                            
                                             <div class="form-group">
                                                <label class="col-sm-3 control-label">photo</label>
                                                <div class="col-sm-9">
                                                    <input type="file" name="photo" class="form-control" value="<?php echo $c->photo ; ?>" />
                                                </div>
                                            </div><!-- form-group -->
                                                                                   
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Nom <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="nom" class="form-control" value="<?php echo $c->nom_client ; ?>" required />
                                                </div>
                                            </div><!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Prenom</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="prenom" class="form-control" value="<?php echo $c->prenom_client ; ?>" required/>
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" >genre</label>
                                                <div class="col-sm-9">
                                                    <select name="genre" class="form-control" value="<?php echo $c->genre ; ?>" required > 
                                                        <option></option>
                                                        <option>Homme</option>
                                                        <option>Femme</option>
                                                    </select>
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Adresse</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="adresse" class="form-control" value="<?php echo $c->adresse_client ; ?>" required/>
                                                </div>
                                            </div><!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Téléphone</label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="tel" class="form-control" value="<?php echo $c->tel_client ; ?>" required/>
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Email <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" value="<?php echo $c->email_client ; ?>" required />
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Mot de passe <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="pass" class="form-control"  value="<?php echo $c->pass_client ; ?>" required>
                                                </div>
                                            </div><!-- form-group -->
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"> Confirmer le Mot de passe <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="passconfirmer" class="form-control" placeholder="Confirmer le Mot de passe" required>
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Ville <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="ville" class="form-control" value="<?php echo $c->ville_client ; ?>"  required>
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Code Postale<span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="poste" class="form-control" value="<?php echo $c->poste ; ?>"  required>
                                                </div>
                                            </div><!-- form-group -->
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Date de naissance <span class="asterisk">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="date" name="dtn" class="form-control"  value="<?php echo $c->dtn ; ?>" required />
                                                </div>
                                            </div><!-- form-group -->
                                            
                                        </div><!-- row -->
                                    </div><!-- panel-body -->
                    <div class="panel-footer">
                        <span class="asterisk" style="text-align: right;">(*) champ obligatoire!</span>
                        <div class="row">

                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="submit" name="modif" value="modifier" class="btn btn-primary mr5">
                                <button type="reset" class="btn btn-dark">Annuler</button>
                            </div>

                        </div>

                    </div><!-- panel-footer -->  
                </div>
             </form>
        </div><!-- panel -->
       
<?php
$i++;
}
?>
</div><!-- col-md-6 -->
</div><!-- contentpanel -->



