<div class="contentpanel">
    <div class="panel panel-primary-head">
        <div class="panel-heading">
            <h4 class="panel-title"  style="text-align: center">Liste des Agents</h4>
        </div><!-- panel-heading -->
        <table id="basicTable" class="table table-striped table-bordered responsive">
            <thead class="">
                <tr>
                    <th  style="text-align: center">CIN</th>
                    <th  style="text-align: center">Photo</th>
                    <th  style="text-align: center">Nom</th>
                    <th  style="text-align: center">Prenom</th>
                    <th  style="text-align: center">Genre</th>
                    <th  style="text-align: center">Télephone</th>
                    <th  style="text-align: center">Email</th>
                    <th  style="text-align: center">Ville</th>
                    <th  style="text-align: center">Code Postale</th>
                    <th  style="text-align: center">Date d'embauche</th>
                    <th  style="text-align: center"> <span class="glyphicon   glyphicon-tasks"></span> </th> 
                </tr>
            </thead>
            <tbody>
			
                <?php
                $i = 0;
                $agent = new agent();
                $clause = "";
                while ($i < compteurTable("agent", $clause)) {
                    $agent->affiche_agent($i, $clause)
               ?>

                    <tr>
			<td><?php echo $agent->cin_agent; ?></td>
                        <td><?php echo "<a href='photoAgent/$agent->photo'><img src='photoAgent/$agent->photo' width=50 height=50></a>"; ?></td>
                        <td><?php echo $agent->nom_agent; ?></td>
                        <td><?php echo $agent->prenom_agent; ?></td>
                        <td><?php echo $agent->genre; ?></td>
                        <td><?php echo $agent->tel_agent; ?></td>
                        <td><?php echo $agent->email_agent; ?></td>
                        <td><?php echo $agent->ville_agent;?></td>
                        <td><?php echo $agent->code_postale; ?></td>
                        <td><?php echo $agent->date_embauche; ?></td>
                        <td><a onclick="return confirm('Vous ete sure?') class='bacground-color:red;'" href="index.php?supp_ag=<?php echo $agent->id_agent ; ?>"> <span class="glyphicon  glyphicon-trash"></span></a> <span> . </span>  <a href="index.php?modif_ag=<?php echo $agent->id_agent ; ?>"> <span class="glyphicon   glyphicon-edit"></span></a></td>
                    </tr>
                    <?php
                    $i++;
                } ?>
                

            </tbody>
        </table>
    </div><!-- panel -->
</div><!-- contentpanel -->
