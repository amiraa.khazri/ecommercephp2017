<div class="contentpanel">
    <div class="panel panel-primary-head">
        <div class="panel-heading">
            <h4 class="panel-title"  style="text-align: center">Liste des produits</h4>
        </div><!-- panel-heading -->
        <table id="basicTable" class="table table-striped table-bordered responsive">
            <thead class="">
                <tr>
                    <th  style="text-align: center">Référance</th>
                    <th  style="text-align: center">Catégorie</th>
                    <th  style="text-align: center">Libéllé</th>
                    <th  style="text-align: center">Photo</th>
                    <th  style="text-align: center">Prix</th>
                    <th  style="text-align: center">Date</th>
                    <th  style="text-align: center">Quantité</th>
                    <th  style="text-align: center">Description</th>
                    <th  style="text-align: center">Marques</th>
                    <th  style="text-align: center">Genre</th>
                    <th  style="text-align: center"><span class="glyphicon   glyphicon-tasks"></span> </th>
               
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                $prod = new produit();
                $clause = "";
                while ($i < compteurTable("produit", $clause)) {
                    $prod->affiche_produit($i, $clause);
					
					$cat=new categorie();
					$cat->affiche_categorie("","where id_categorie='".$prod->id_categorie."'");
                    ?>

                    <tr  style="text-align: center">
                        <td><?php echo $prod->ref_produit; ?></td>
                        <td><?php echo $cat->categorie; ?></td>
                        <td><?php echo $prod->libelle; ?></td>
                        <td><?php echo "<a href='photoproduit/$prod->photo'><img src='photoproduit/$prod->photo' width=50 height=50></a>"; ?></td>
                        <td><?php echo $prod->prix; ?>DT</td>
                        <td><?php echo $prod->date; ?></td>
                        <td><?php echo $prod->quantite; ?></td>
                        <td><?php echo $prod->description; ?></td>
                        <td><?php echo $prod->marque; ?></td>
                        <td><?php echo $prod->Genre; ?></td>
                        <td><a onclick="return confirm('Voullez vous vraiment le supprimer')" href="index.php?supp_prod=<?php echo $prod->id ; ?>" title="Supprimer" data-toggle="span" data-trigger="hover" > <span class="glyphicon  glyphicon-trash"></span></a>.<a href="index.php?modif_prod=<?php echo $prod->id ; ?>" title="Modifier" data-toggle="span" data-trigger="hover"> <span class="glyphicon   glyphicon-edit"></span></a></td>
                        
                    </tr>
                    <?php
                    $i++;
                }
                ?>

            </tbody>
        </table>
    </div><!-- panel -->
</div><!-- contentpanel -->
