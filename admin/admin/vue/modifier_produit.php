            
<?php
if (isset($_POST['modif'])) {

    $content_dir = 'photoproduit/'; // dossier où sera déplacé le fichier

    $tmp_file = $_FILES['photo']['tmp_name'];

    if (!is_uploaded_file($tmp_file)) {
        exit("Le fichier est introuvable");
    }


    // on copie le fichier dans le dossier de destination
    $name_file = $_FILES['photo']['name'];

    if (!move_uploaded_file($tmp_file, $content_dir . $name_file)) {
        exit("Impossible de copier le fichier dans $content_dir");
    }

    $photo = $_FILES['photo']['name'];

    $prod = new produit(NULL, $_POST['ref'], $_POST['cat'], $_POST['libelle'], $photo, $_POST['prix'], $_POST['date'], $_POST['quantite'], $_POST['desc'], $_POST['marque'], $_POST['Genre']);
    $id = $_GET["modif_prod"];
    $prod->modifier_produit($id);
}
$i = 0;
$p = new produit();
$clause = "where id='" . $_GET['modif_prod'] . "'";
while ($i < compteurtable("produit", $clause)) {
    $p->affiche_produit($i, $clause);
    ?>
    <div class="contentpanel">

        <div class="row">
            <div class="col-md-8 col-sm-offset-2 col-xs-12 ">
                <form method="post" enctype="multipart/form-data">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" style="text-align: center">Modifier Produit</h4>
                        </div><!-- panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Reférance produit <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="ref" class="form-control" value="<?php echo $p->ref_produit; ?>" placeholder="Réferance.." required="" oninvalid="this.setCustomValidity('vous avez oublier de remplire le champs reference')" />
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Catégorie <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <select name="cat" class="form-control" required />
                                        <option></option>
    <?php
    $i = 0;
    $c = new categorie();

    while ($i < compteurTable("categorie", "")) {
        $c->affiche_categorie($i, "");
        ?>

                                            <option  value="<?php echo $c->id_categorie; ?> "><?php echo $c->categorie; ?> </option>
                                            <?php
                                            $i++;
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Libellé <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text" name="libelle" class="form-control" value="<?php echo $p->libelle; ?>" placeholder="Libellé.." required />
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">photo</label>
                                    <div class="col-sm-9">
                                        <input type="file" name="photo"  value="<?php echo $p->photo; ?>" class="form-control" />
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Prix</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="prix" value="<?php echo $p->prix; ?>" class="form-control" />
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">date</label>
                                    <div class="col-sm-9">
                                        <input type="date" name="date" value="<?php echo $p->date; ?>" class="form-control" />
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Quantité</label>
                                    <div class="col-sm-9">
                                        <input type="number" name="quantite" value="<?php echo $p->quantite; ?>" class="form-control" required/>
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">description <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <textarea rows="5" class="form-control" value="<?php echo $p->description; ?>" name="desc" placeholder="Enter votre description..." required></textarea>
                                    </div>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Marque <span class="asterisk">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="text"  class="form-control"  value="<?php echo $p->marque; ?>" name="marque" required>
                                    </div>
                                </div><!-- form-group -->
                                
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Genre <span class="asterisk">*</span></label>
                                    	<div class="col-sm-9">
                                                    <select name="Genre" class="form-control" value="<?php echo $p->Genre; ?> "required />
                                                    <option></option>
                                                    <option>Homme</option>
                                                    <option>Femme</option>
                                                    <option>Gamins</option>
                                                    </select>
                                                </div>
                                    
                                   
                                
                            </div><!-- row -->
                        </div><!-- panel-body -->
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <input type="submit" name="modif" value="Modifier" class="btn btn-primary mr5">
                                    <button type="reset" class="btn btn-dark">Reset</button>
                                </div>
                            </div>
                        </div><!-- panel-footer -->  
                    </div><!-- panel -->
                </form>
    <?php
    $i++;
}
?>
        </div><!-- col-md-6 -->
    </div><!-- contentpanel -->
</div><!-- mainpanel -->




