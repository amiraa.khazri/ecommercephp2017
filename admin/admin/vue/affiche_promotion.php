<div class="contentpanel">
    <div class="panel panel-primary-head">
        <div class="panel-heading" style="text-align: center">
            <h4 class="panel-title">Les Promotion</h4>
        </div><!-- panel-heading -->
        <table id="basicTable" class="table table-striped table-bordered responsive">
            <thead class="">
                <tr>
                    <th  style="text-align: center">Référance</th>
                    <th  style="text-align: center">Prix</th>
                    <th  style="text-align: center">Date debut</th>
                    <th  style="text-align: center">Date fin</th>
                    <th  style="text-align: center"><span class="glyphicon   glyphicon-tasks"></span> </th>


                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                $promo = new promotion();
                $clause = "";
                while ($i < compteurTable("promotion", $clause)) {
                    $promo->affiche_promotion($i, $clause)
                    ?>

                    <tr style="text-align: center">

                        <td><?php echo $promo->ref_produit; ?></td>
                        <td><?php echo $promo->prix; ?></td>
                        <td><?php echo $promo->date_debut_promo; ?></td>
                        <td><?php echo $promo->date_fin_promo; ?></td>
                        <td><a onclick="return confirm('Voullez vous vraiment le supprimer')" href="index.php?supp_promo=<?php echo $promo->id_promotion ; ?>" title="Supprimer" data-toggle="span" data-trigger="hover" > <span class="glyphicon  glyphicon-trash"></span></a>.<a href="index.php?modif_promo=<?php echo $promo->id_promotion ; ?>" title="Modifier" data-toggle="span" data-trigger="hover"> <span class="glyphicon   glyphicon-edit"></span></a></td>
                      

                    </tr>
                    <?php
                    $i++;
                }
                ?>

            </tbody>
        </table>
    </div><!-- panel -->
</div><!-- contentpanel -->
