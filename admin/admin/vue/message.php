
                    
                    <div class="contentpanel">
                        <div class="row">
                            <div class="col-sm-3 col-md-3 col-lg-2">
                               <a href="vue/nouveauMsg.php" class="btn btn-danger btn-block btn-create-msg">Nouveau Message</a>
                               <br/>
                            </div>
                            
                            <div class="col-sm-9 col-md-9 col-lg-10">
                                
                                <div class="msg-header">
                                    <div class="pull-right">
                                        <button class="btn btn-white tooltips" type="button" data-toggle="tooltip" title="Supprimer Tous les messages">
                                        <i class="fa fa-trash-o"></i></button> 
                                    </div>
                                    <div class="pull-left">
                                     
                                                                      
                                        
                                        <div class="btn-group">
                                           
                                        </div>
                                       
                                    </div><!-- pull-right -->
                                </div><!-- msg-header -->
                                
                                
                                <ul class="media-list msg-list" style="margin-left:-208px !important">
                                    <li class="media unread">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox1">
                                            <label for="checkbox1"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-online" src="images/photos/user1.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <i class="fa fa-paperclip mr5"></i>
                                                <small>Yesterday 5:51am</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4 class="sender">Renov Leonga</h4>
                                            <p><a href="view_message.html"><strong class="subject">Hi Hello!</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</a></p>
                                        </div>
                                    </li>
                                    <li class="media unread">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox2">
                                            <label for="checkbox2"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-offline" src="images/photos/user2.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <small>July 10 1:10pm</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div> 
                                            </div>
                                            <h4 class="sender">Weno Carasbong</h4>
                                            <p><a href="view_message.html"><strong class="subject">Weekly Recognition Reminder</strong> Consectetur adipisicing elit, sed do eiusmod tempor incididunt...</a></p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox3">
                                            <label for="checkbox3"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-online" src="images/photos/user3.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <i class="fa fa-paperclip mr5"></i>
                                                <small>July 10 11:00am</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4 class="sender">Zaham Sindilmaca</h4>
                                            <p><a href="view_message.html"><strong class="subject">Weno added you as a friend</strong> Consectetur adipisicing elit, sed do eiusmod tempor incididunt...</a></p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox4">
                                            <label for="checkbox4"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-online" src="images/photos/user4.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <i class="fa fa-paperclip mr5"></i>
                                                <small>July 9 4:10am</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4 class="sender">Vinal Biguitlam</h4>
                                            <p><a href="view_message.html"><strong class="subject">Mobile Banking Transaction</strong> Consectetur adipisicing elit, sed do eiusmod tempor incididunt...</a></p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox5">
                                            <label for="checkbox5"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-offline" src="images/photos/user5.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <small>July 8 1:10pm</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4 class="sender">Perturo Ranodu</h4>
                                            <p><a href="view_message.html"><strong class="subject">Brown Bag Reminder</strong> Sed do eiusmod tempor incididunt consectetur adipisicing elit, sed do...</a></p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox6">
                                            <label for="checkbox6"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-online" src="images/photos/user1.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <i class="fa fa-paperclip mr5"></i>
                                                <small>July 7 5:51am</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4 class="sender">Renov Leonga</h4>
                                            <p><a href="view_message.html"><strong class="subject">Hi Hello!</strong> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</a></p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox7">
                                            <label for="checkbox7"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-offline" src="images/photos/user2.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <i class="fa fa-paperclip mr5"></i>
                                                <small>July 7 1:10pm</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div> 
                                            </div>
                                            <h4 class="sender">Weno Carasbong</h4>
                                            <p><a href="view_message.html"><strong class="subject">Weekly Recognition Reminder</strong> Consectetur adipisicing elit, sed do eiusmod tempor incididunt...</a></p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox8">
                                            <label for="checkbox8"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-offline" src="images/photos/user3.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <small>July 6 11:00am</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4 class="sender">Zaham Sindilmaca</h4>
                                            <p><a href="view_message.html"><strong class="subject">Weno added you as a friend</strong> Consectetur adipisicing elit, sed do eiusmod tempor incididunt...</a></p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox9">
                                            <label for="checkbox9"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-offline" src="images/photos/user4.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <small>July 6 4:10am</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4 class="sender">Vinal Biguitlam</h4>
                                            <p><a href="view_message.html"><strong class="subject">Mobile Banking Transaction</strong> Consectetur adipisicing elit, sed do eiusmod tempor incididunt...</a></p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <div class="ckbox ckbox-primary pull-left">
                                            <input type="checkbox" id="checkbox10">
                                            <label for="checkbox10"></label>
                                        </div>
                                        <a class="pull-left" href="#">
                                            <img class="media-object img-circle img-online" src="images/photos/user5.png" alt="...">
                                        </a>
                                        <div class="media-body">
                                            <div class="pull-right media-option">
                                                <small>July 6 1:10pm</small>
                                                <a href="#"><i class="fa fa-star"></i></a>
                                                <div class="btn-group">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <i class="fa fa-cog"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                        <li><a href="#">Mark as Unread</a></li>
                                                        <li><a href="#">Reply</a></li>
                                                        <li><a href="#">Forward</a></li>
                                                        <li><a href="#">Archive</a></li>
                                                        <li><a href="#">Move to Folder</a></li>
                                                        <li><a href="#">Delete</a></li>
                                                        <li class="divider"></li>
                                                        <li><a href="#">Report as Spam</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <h4 class="sender">Perturo Ranodu</h4>
                                            <p><a href="view_message.html"><strong class="subject">Brown Bag Reminder</strong> Sed do eiusmod tempor incididunt consectetur adipisicing elit, sed do...</a></p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div><!-- contentpanel -->
                    
                