<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>E-shop</title>

        <link href="../css/style.default.css" rel="stylesheet">
        
        	<script src="../../../dist/sweetalert.min.js"></script>
	<script rel="stylesheet" type="text/css" href="../../../dist/sweetalert.css"></script>

       
    </head>

    <body style="background-color:#BBACAC; important!">
        
        
        <section>
            
				<div class="panel panel-signup">
						<div class="panel-body">
							<div class="logo text-center">
								<img src="../images/lo.png" alt="E-shop Logo" >
							</div>
							<br />
							<h4 class="text-center mb5">Creer un nouveau message</h4>
							<p class="text-center"> ci-dessous</p>
							
							<div class="mb30" ></div>
								
								<form action="../index.php?id=aff_message" method="post">
									<br />
									<div class="row" style="width:1000px !important">
										<div class="col-sm-6">
											<div class="input-group mb15">
												<span class="input-group-addon">@</span>
												<input type="email" class="form-control" placeholder="Enter Email Address" required>
											</div><!-- input-group -->
                                     </div><!-- row -->
                                     <div class="row" style="left: 200px !important">
										</div>
										<div class="col-sm-6">
											<div class="input-group mb15">
												<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                                <textarea class="form-control" cols="100" rows="6" required></textarea>
												<!--<input type="password" class="form-control" placeholder="Enter Password">-->
											</div><!-- input-group -->
										</div>
									</div><!-- row -->
									
									<br />
                                    
							
							
						</div><!-- panel-body -->
                        
						<div class="panel-footer" >
							<input  type="submit"  name="btn" class="btn btn-primary btn-rounded btn-lg text-center " style="margin-left:350px!important" value="Envoyer"/>
						</div><!-- panel-footer -->
					</div><!-- panel --></form>
 

        </section>


			<script src="../js/jquery-1.11.1.min.js"></script>
			<script src="../js/jquery-migrate-1.2.1.min.js"></script>
			<script src="../js/bootstrap.min.js"></script>
			<script src="../js/modernizr.min.js"></script>
			<script src="../js/pace.min.js"></script>
			<script src="../js/retina.min.js"></script>
			<script src="../js/jquery.cookies.js"></script>
			<script src="../js/custom.js"></script>

    </body>
</html>
