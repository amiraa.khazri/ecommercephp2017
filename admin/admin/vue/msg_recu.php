<?php
$notif = new contact();
$notif->notif("admin@admin.fr");
?>

                    <div class="contentpanel">
                        <div class="panel panel-primary-head">
                            <div class="panel-heading">
                                <h4 class="panel-title" style="text-align: center">Liste des messages reçus</h4>
                            </div><!-- panel-heading -->
                            <table id="basicTable" class="table table-striped table-bordered responsive">
                                <thead class="">
                <tr>
                    <th  style="text-align: center">Expéditeur</th>
                    <th  style="text-align: center">Sujet</th>
                    <th  style="text-align: center">Date</th>
                    <th  style="text-align: center">Message</th>
                    <th  style="text-align: center"><span class="glyphicon   glyphicon-tasks"></span></th>
                </tr>
                                </thead>
                                <tbody>
                                <?php
								  $i=0;
								  $c=new contact();
								  $clause="where distinataire='admin@admin.fr'";
								 while($i<compteurTable("contact",$clause))
								  {
									$c->affiche_contact($i,$clause)
								?>
                                
                                    <tr>
                                      
                                        <td style="text-align: center"><?php echo $c->expediteur; ?></td>
                                        <td style="text-align: center"><?php echo $c->sujet; ?></td>
                                        <td style="text-align: center"><?php echo $c->date; ?></td>
                                        <td style="text-align: center"><?php echo $c->message; ?></td>
                                        <td style="text-align: center"><a title="Supprimer" data-toggle="span" data-trigger="hover"  onclick="return confirm('Voullez vous vraiment supprimer')" href="index.php?supp_contact_rec=<?php echo $c->id ; ?>"> <span class="glyphicon  glyphicon-trash"></span></a></td>
                             
                                    </tr>
                                  <?php
								  $i++;
								  }
								  ?>
                                    
                                </tbody>
                            </table>
                        </div><!-- panel -->
                    </div><!-- contentpanel -->
