            
<?php
if (isset($_POST['modif'])) {
    
        $content_dir = 'photoAgent/'; // dossier où sera déplacé le fichier

        $tmp_file = $_FILES['photo']['tmp_name'];

        if (!is_uploaded_file($tmp_file)) {
            exit("Le fichier est introuvable");
        }


        // on copie le fichier dans le dossier de destination
        $name_file = $_FILES['photo']['name'];

        if (!move_uploaded_file($tmp_file, $content_dir . $name_file)) {
            exit("Impossible de copier le fichier dans $content_dir");
        }

        $photo = $_FILES['photo']['name'];

        $agent = new agent(NULL, $_POST['cin'], $photo, $_POST['nom'], $_POST['prenom'], $_POST['genre'], $_POST['tel'], $_POST['email'], $_POST['mdpass'], $_POST['ville'], $_POST['poste'], $_POST['dt']);
        $id=$_GET["modif_ag"];
		$agent->modifier_agent($id);
        ?>
        
}
$i=0;
$c=new agent();
$clause="where id_agent='".$_GET['modif_ag']."'";
while($i<compteurtable("agent",$clause))
{
	$c->affiche_agent($i,$clause);

?>
<div class="contentpanel">

    <div class="row">
        <div class="col-md-8 col-sm-offset-2 col-xs-12 ">
            <form method="post" enctype="multipart/form-data">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title"  style="text-align: center">Modifier des Agents</h4>
                    </div><!-- panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">CIN <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="cin" class="form-control" value="<?php echo $c->cin_agent ; ?>" placeholder="CIN.." required="" oninvalid="this.setCustomValidity('vous avez oublier de remplire le champs reference')" />
                                </div>
                            </div><!-- form-group -->


                            <div class="form-group">
                                <label class="col-sm-3 control-label">photo <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="file" name="photo" class="form-control" value="<?php echo $c->photo ; ?>" />
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nom Agent <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="nom" class="form-control" value="<?php echo $c->nom_agent ; ?>" />
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Prenom Agent <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" name="prenom" class="form-control" value="<?php echo $c->prenom_agent ; ?>" />
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">genre <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <select name="genre" class="form-control" required value="<?php echo $c->genre ; ?>"/> 
                                    <option>  </option>
                                    <option> Homme </option>
                                    <option> Femme </option>
                                    </select>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Téléphone <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="tel" placeholder="Enter le Telephone..." value="<?php echo $c-tel_agent ; ?>" required/>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Email <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" class="form-control" value="<?php echo $c->email_agent ; ?>"  required>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Mot de Passe <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="mdpass" value="<?php echo $c->pass_agent ; ?>" placeholder="Entre le mot de passe..." required/>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Ville <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" value="<?php echo $c->ville_agent ; ?>" name="ville" placeholder="Entre la ville..." required/>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Code Postale <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="poste" value="<?php echo $c->code_postale ; ?>" placeholder="Entre le code postale..." required/>
                                </div>
                            </div><!-- form-group -->

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Date d'embauche <span class="asterisk">*</span></label>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" name="dt" value="<?php echo $c->date_embauche ; ?>" placeholder="Entre la dare d'embauche..." required/>
                                </div>
                            </div><!-- form-group -->

                        </div><!-- row -->
                    </div><!-- panel-body -->
                    <div class="panel-footer">
                        <span class="asterisk" style="text-align: right;">(*) champ obligatoire!</span>
                        <div class="row">

                            <div class="col-sm-9 col-sm-offset-3">
                                <input type="submit" name="modif" value="modifier" class="btn btn-primary mr5">
                                <button type="reset" class="btn btn-dark">Annuler</button>
                            </div>

                        </div>

                    </div><!-- panel-footer -->  
                </div>
        </div><!-- panel -->
        </form>
<?php
$i++;
}
?>
    </div><!-- col-md-6 -->
</div><!-- contentpanel -->


