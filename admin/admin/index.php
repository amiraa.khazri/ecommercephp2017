<?php
session_start();
if (!isset($_SESSION['log'])) {
    header("location:../index.php");
} else {
    include("../config/fonction.php");
    include("models/produit.php");
    include("models/categorie.php");
    include("models/promotion.php");
    include("models/agent.php");
    include("models/client.php");
    include("models/admin.php");
    include("models/contact.php");
    include("models/commande.php");

    connect();
	$name=$_SESSION['log'];
	$date=date("d-m-Y");
    $heure=date("H:i:s");
    ?>
    <!DOCTYPE html>
    <html lang="en">

        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>Page administratif</title>

            <link href="css/style.default.css" rel="stylesheet">
            <link href="css/morris.css" rel="stylesheet">
            <link href="css/select2.css" rel="stylesheet" />
             <link href="cdn.datatables.net/responsive/1.0.1/css/dataTables.responsive.css" rel="stylesheet">
           

        </head>

        <body>

            <header>
                <div class="headerwrapper" >
                    <div class="header-left" >
                        <a href="index.html" class="logo" style="margin-top:-2px;">
                            <img src="images/shop1.png" alt="LOGO"  style="width:120px; height:30px; "/> 
                        </a>
                        <div class="pull-right">
                            <a href="#" class="menu-collapse">
                                <i class="fa fa-bars"></i>
                            </a>
                        </div>
                    </div><!-- header-left -->

                    <div class="header-right">

                        <div class="pull-right">
                            <div class="btn-group btn-group-list btn-group-notification">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="badge">5</span>
                                </button>
                                <div class="dropdown-menu pull-right">
                                    <a href="#" class="link-right"><i class="fa fa-search"></i></a>
                                    <h5>Notification</h5>
                                    <ul class="media-list dropdown-list">
                                        <li class="media">
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user1.png" alt="">
                                            <div class="media-body">
                                                <strong>Nusja Nawancali</strong> likes a photo of you
                                                <small class="date"><i class="fa fa-thumbs-up"></i> 15 minutes ago</small>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user2.png" alt="">
                                            <div class="media-body">
                                                <strong>Weno Carasbong</strong> shared a photo of you in your <strong>Mobile Uploads</strong> album.
                                                <small class="date"><i class="fa fa-calendar"></i> July 04, 2014</small>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user3.png" alt="">
                                            <div class="media-body">
                                                <strong>Venro Leonga</strong> likes a photo of you
                                                <small class="date"><i class="fa fa-thumbs-up"></i> July 03, 2014</small>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user4.png" alt="">
                                            <div class="media-body">
                                                <strong>Nanterey Reslaba</strong> shared a photo of you in your <strong>Mobile Uploads</strong> album.
                                                <small class="date"><i class="fa fa-calendar"></i> July 03, 2014</small>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user1.png" alt="">
                                            <div class="media-body">
                                                <strong>Nusja Nawancali</strong> shared a photo of you in your <strong>Mobile Uploads</strong> album.
                                                <small class="date"><i class="fa fa-calendar"></i> July 02, 2014</small>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="dropdown-footer text-center">
                                        <a href="#" class="link">See All Notifications</a>
                                    </div>
                                </div><!-- dropdown-menu -->
                            </div><!-- btn-group -->

                            <div class="btn-group btn-group-list btn-group-messages">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge">2</span>
                                </button>
                                <div class="dropdown-menu pull-right">
                                    <a href="#" class="link-right"><i class="fa fa-plus"></i></a>
                                    <h5>New Messages</h5>
                                    <ul class="media-list dropdown-list">
                                        <li class="media">
                                            <span class="badge badge-success">New</span>
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user1.png" alt="">
                                            <div class="media-body">
                                                <strong>Nusja Nawancali</strong>
                                                <p>Hi! How are you?...</p>
                                                <small class="date"><i class="fa fa-clock-o"></i> 15 minutes ago</small>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <span class="badge badge-success">New</span>
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user2.png" alt="">
                                            <div class="media-body">
                                                <strong>Weno Carasbong</strong>
                                                <p>Lorem ipsum dolor sit amet...</p>
                                                <small class="date"><i class="fa fa-clock-o"></i> July 04, 2014</small>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user3.png" alt="">
                                            <div class="media-body">
                                                <strong>Venro Leonga</strong>
                                                <p>Do you have the time to listen to me...</p>
                                                <small class="date"><i class="fa fa-clock-o"></i> July 03, 2014</small>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user4.png" alt="">
                                            <div class="media-body">
                                                <strong>Nanterey Reslaba</strong>
                                                <p>It might seem crazy what I'm about to say...</p>
                                                <small class="date"><i class="fa fa-clock-o"></i> July 03, 2014</small>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="img-circle pull-left noti-thumb" src="images/photos/user1.png" alt="">
                                            <div class="media-body">
                                                <strong>Nusja Nawancali</strong>
                                                <p>Hey I just met you and this is crazy...</p>
                                                <small class="date"><i class="fa fa-clock-o"></i> July 02, 2014</small>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="dropdown-footer text-center">
                                        <a href="#" class="link">See All Messages</a>
                                    </div>
                                </div><!-- dropdown-menu -->
                            </div><!-- btn-group -->

                            <div class="btn-group btn-group-option">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
				<li><a href="vue/nouveauCompte.html"><i class="glyphicon glyphicon-user"></i> Ajouter Admin</a></li>
				<li><a href="vue/MonProfile.html"><i class="glyphicon glyphicon-cog"></i>Mon Compte</a></li>
                                    <li><a href="deconnect.php"><i class="glyphicon glyphicon-log-out"></i>Deconnecter</a></li>
                                </ul>
                            </div><!-- btn-group -->

                        </div><!-- pull-right -->

                    </div><!-- header-right -->

                </div><!-- headerwrapper -->
            </header>

            <section>
                <div class="mainwrapper">
                    <div class="leftpanel">
                        <div class="media profile-left">
                            <a class="pull-left profile-thumb" href="index.php">
                                <img class="img-circle" src="images/photos/profile.png" alt="">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Administrateur</h4>
                                <small class="text-muted"><?php echo $name;?></small>
                            </div>
                        </div><!-- media -->
                        <ul class="nav nav-pills nav-stacked">
                            <li class="parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Client</span></a>
                                <ul class="children">
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=ajt_client">Ajouter</a></li>
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=aff_client">Afficher</a></li>
                                </ul>
                            </li>
                            
                            <li class="parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Agent</span></a>
                                <ul class="children">
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=ajt_agent">Ajouter</a></li>
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=aff_agent">Afficher</a></li>
                                </ul>
                            </li>
                             
                            <li class="parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Catégorie</span></a>
                                <ul class="children">
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=ajt_categorie">Ajouter</a></li>
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=aff_categorie">Afficher</a></li>
                                </ul>
                            </li>
                            
                            <li class="parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Produit</span></a>
                                <ul class="children">
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=ajt_prod">Ajouter</a></li>
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=aff_prod">Afficher</a></li>
                                </ul>
                            </li>
                            <li class="parent"><a href="#"><i class="fa fa-suitcase"></i> <span>Promotion</span></a>
                                <ul class="children">
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=ajt_promotion">Ajouter</a></li>
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=aff_promotion">Afficher</a></li>
                                </ul>
                            </li>
                            
                            <li class="parent"><a href="#"><i class="fa fa-bars"></i> <span>Commandes</span></a>
                                <ul class="children">
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=cmd">Liste Commandes</a></li>
                                </ul>
                            </li>
                             <li class="parent"><a href="#"><i class="fa fa-envelope-o"></i> <span>Messages</span> <span style="background-color:#03F;color:white;font-weight:bold;border-radius:30px;box-shadow:1px 1px 1px gray;padding:2px 3px 2px 4px;top: -6px;right:-6px;font-size:1em;"><?php echo compteurTable("contact","where distinataire='admin@admin.fr' and notification='1'" ) ; ?></span></a>
                                <ul class="children">
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=nouveau_msg">Nouveau Message</a></li>
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=msg_env">Messages Envoyr</a></li>
                                    <li style="background-color:#E9E9E9 ;!important"><a href="index.php?id=msg_recu">Messages réçus</a></li>
                                </ul>
                            </li>
                            
                              <li><a href="index.php?id=stat"> <i class="fa fa-map-marker"></i> <span>statistique</span></a> </li>
                              <li class="parent"><a href="#"> <i class="fa fa-map-marker"></i> <span>Maps</span></a>
                                <ul class="children">
                                    <li style="background-color:#E9E9E9 ;!important"><a href="#" >Notre Position</a></li>
                                </ul>
                            </li>
                         

                        </ul>
                        
                        
                       

                    </div><!-- leftpanel -->

                    <div class="mainpanel">

                        <div class="contentpanel">

                            <div class="row row-stat">
                                <div class="col-md-4">
                                    <div class="panel panel-success-alt noborder">
                                        <div class="panel-heading noborder">
                                            <div class="panel-btns">
                                                <a href="#" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                                            </div><!-- panel-btns -->
                                            <div class="panel-icon"><i class="fa fa-dollar"></i></div>
                                            <div class="media-body">
                                                <h5 class="md-title nomargin">Nombre des commandes</h5>
                                                <h1 class="mt5"><?php echo compteurTable("client","") ; ?></h1>
                                            </div><!-- media-body -->
                                            <hr>
                                            <div class="clearfix mt20">
                                               <div class="pull-left">
                                                    <h5 class="md-title nomargin">Ajourd'hui</h5>
                                                  <h5 class="nomargin"><?php echo $date ; ?></h5>
                                                </div>
                                                
                                                <div class="pull-right">
                                                <h5 class="md-title nomargin">Heure</h5>
                                                <h5 class="nomargin"><?php echo $heure;?></h5>
                                                </div>
                                            </div>

                                        </div><!-- panel-body -->
                                    </div><!-- panel -->
                                </div><!-- col-md-4 -->

                                <div class="col-md-4">
                                    <div class="panel panel-primary noborder">
                                        <div class="panel-heading noborder">
                                            <div class="panel-btns">
                                                <a href="#" class="panel-close tooltips" data-toggle="tooltip" title="Close Panel"><i class="fa fa-times"></i></a>
                                            </div><!-- panel-btns -->
                                            <div class="panel-icon"><i class="fa fa-users"></i></div>
                                            <div class="media-body">
                                                <h5 class="md-title nomargin">Nouveau compte utlilsateurs</h5>
                                                <h1 class="mt5">138,102</h1>
                                            </div><!-- media-body -->
                                            <hr>
                                            <div class="clearfix mt20">
                                               <div class="pull-left">
                                                    <h5 class="md-title nomargin">Ajourd'hui</h5>
                                                  <h5 class="nomargin"><?php echo $date ; ?></h5>
                                                </div>
                                                 <div class="pull-right">
                                                <h5 class="md-title nomargin">Heure</h5>
                                                <h5 class="nomargin"><?php echo $heure;?></h5>
                                                </div>
                                            </div>

                                        </div><!-- panel-body -->
                                    </div><!-- panel -->
                                </div><!-- col-md-4 -->

                                <div class="col-md-4">
                                    <div class="panel panel-dark noborder">
                                        <div class="panel-heading noborder">
                                            <div class="panel-btns">
                                                <a href="#" class="panel-close tooltips" data-toggle="tooltip" data-placement="left" title="Close Panel"><i class="fa fa-times"></i></a>
                                            </div><!-- panel-btns -->
                                            <div class="panel-icon"><i class="fa fa-pencil"></i></div>
                                            <div class="media-body">
                                                <h5 class="md-title nomargin">Nouvaux Posts agents</h5>
                                                <h1 class="mt5">153,900</h1>
                                            </div><!-- media-body -->
                                            <hr>
                                            <div class="clearfix mt20">
                                                <div class="pull-left">
                                                    <h5 class="md-title nomargin">Ajourd'hui</h5>
                                                  <h5 class="nomargin"><?php echo $date ; ?></h5>
                                                </div>
                                                 <div class="pull-right">
                                                <h5 class="md-title nomargin">Heure</h5>
                                                <h5 class="nomargin"><?php echo $heure;?></h5>
                                                </div>
                                            </div>

                                        </div><!-- panel-body -->
                                    </div><!-- panel -->
                                </div><!-- col-md-4 -->
                            </div>
                        </div><!-- contentpanel -->
    <?php
    if(isset($_GET["id"])) {

        if($_GET['id']=="ajt_prod") include ("vue/ajout_produit.php");
        if($_GET['id']== "aff_prod") include ("vue/affiche_produit.php");
        if($_GET['id']== "ajt_categorie") include ("vue/ajout_categorie.php");
        if($_GET['id']== "aff_categorie") include ("vue/affiche_categorie.php");
        if($_GET['id']== "ajt_promotion") include ("vue/ajout_promotion.php");
        if($_GET['id']== "aff_promotion") include ("vue/affiche_promotion.php");
        if($_GET['id']== "ajt_agent") include ("vue/ajout_agent.php");
        if($_GET['id']== "aff_agent") include ("vue/affiche_agent.php");
        if($_GET['id']== "ajt_client") include ("vue/ajout_client.php");
        if($_GET['id']== "aff_client") include ("vue/affiche_client.php");
        if($_GET['id']== "ajt_admin") include ("vue/ajoutAdmin.php");
		if($_GET['id']== "aff_message") include ("vue/message.php");
		if($_GET['id']== "cmd") include ("vue/liste_commande.php");
		if($_GET['id']== "nouveau_msg") include ("vue/nouveau_msg.php");
		if($_GET['id']== "msg_env") include ("vue/msg_envoyer.php");
		if($_GET['id']== "msg_recu") include ("vue/msg_recu.php");
		if($_GET['id']== "stat") include ("vue/statistique.php");

       
    }
	// sup/modif agent
else if(isset($_GET["supp_ag"]))
{
	$c= new agent();
	$id=$_GET["supp_ag"];
	$c->supprimer_agent($id);	
}
else if(isset($_GET["modif_ag"]))
{
	include("vue/modifier_agent.php");

} 
//supp/modif cleint
else if(isset($_GET["supp_client"]))
{
	$cl= new client();
	$id=$_GET["supp_client"];
	$cl->supprimer_client($id);
}
else if(isset($_GET["modif_clt"]))
{
	include("vue/modifier_client.php");
}
//supp/modif categorie
else if(isset($_GET["supp_cat"]))
{
	$cat= new categorie();
	$id=$_GET["supp_cat"];
	$cat->supprimer_categorie($id);
}
else if(isset($_GET["modif_cat"]))
{
	include("vue/modifier_categorie.php");
}
//supp/modif produit

else if(isset($_GET["supp_prod"]))
{
	$prd= new produit();
	$id=$_GET["supp_prod"];
	$prd->supprimer_produit($id);
}
else if(isset($_GET["modif_prod"]))
{
	include("vue/modifier_produit.php");
}
//supp/modif promo

else if(isset($_GET["supp_promo"]))
{
	$prom= new promotion();
	$id=$_GET["supp_promo"];
	$prom->supprimer_promotion($id);
}
else if(isset($_GET["supp_contact_rec"]))
{
	$c= new contact();
	$id=$_GET["supp_contact_rec"];
	$c->supprimer_contact1($id);
}
else if(isset($_GET["supp_contact_env"]))
{
	$c= new contact();
	$id=$_GET["supp_contact_env"];
	$c->supprimer_contact2($id);
}
else if(isset($_GET["modif_promo"]))
{
	include("vue/modifier_promo.php");
}
else if(isset($_GET["id"]))
{
	$cmd = new commande();
	$cmd->affiche_commande("","where id_commande='".$_GET['id']."'");
	
	$p = new produit();
	$p->affiche_produit("","where ref_produit='".$cmd->ref_produit."'");
	$reste=$p->quantite-$cmd->quantite;
	
	$p->modifier_quantite($cmd->ref_produit,$reste);
	
	$cmd->valider($_GET['id']);
     echo"<script>document.location.replace('index.php?id=cmd');</script>";
}
else
{
   include ("vue/statistique.php");
}

    ?>
                    </div><!-- mainpanel -->
                </div><!-- mainwrapper -->
            </section>

			<script src="js/jequeryMain.js"></script>
            <script src="js/jquery-1.11.1.min.js"></script>
            <script src="js/jquery-migrate-1.2.1.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/modernizr.min.js"></script>
            <script src="js/pace.min.js"></script>
            <script src="js/retina.min.js"></script>
            <script src="js/jquery.cookies.js"></script>

            <script src="js/flot/jquery.flot.min.js"></script>
            <script src="js/flot/jquery.flot.resize.min.js"></script>
            <script src="js/flot/jquery.flot.spline.min.js"></script>
            <script src="js/jquery.sparkline.min.js"></script>
            <script src="js/morris.min.js"></script>
            <script src="js/raphael-2.1.0.min.js"></script>
            <script src="js/bootstrap-wizard.min.js"></script>
            <script src="js/select2.min.js"></script>
            
            
            <script src="js/custom.js"></script>
            <script src="js/dashboard.js"></script>
            
            
            

            <?php
        }
        ?>
        
           

    </body>
</html>
