-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 28 Mai 2018 à 16:01
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `shop`  
/*'''remplacer `amira` par `shop`'''*/
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `login` varchar(100) NOT NULL,
  `mdp` varchar(100) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`login`, `mdp`) VALUES
('amira', 'amira');

-- --------------------------------------------------------

--
-- Structure de la table `agent`
--

CREATE TABLE IF NOT EXISTS `agent` (
  `id_agent` int(11) NOT NULL AUTO_INCREMENT,
  `cin_agent` int(8) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `nom_agent` varchar(100) NOT NULL,
  `prenom_agent` varchar(100) NOT NULL,
  `genre` varchar(10) NOT NULL,
  `tel_agent` varchar(100) NOT NULL,
  `email_agent` varchar(100) NOT NULL,
  `pass_agent` varchar(100) NOT NULL,
  `ville_agent` varchar(100) NOT NULL,
  `code_postale` varchar(100) NOT NULL,
  `date_embauche` varchar(10) NOT NULL,
  PRIMARY KEY (`id_agent`),
  UNIQUE KEY `cin_agent` (`cin_agent`,`email_agent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `agent`
--

INSERT INTO `agent` (`id_agent`, `cin_agent`, `photo`, `nom_agent`, `prenom_agent`, `genre`, `tel_agent`, `email_agent`, `pass_agent`, `ville_agent`, `code_postale`, `date_embauche`) VALUES
(4, 11121, '12.jpeg', 'mira11111', 'mira11111', 'Femme', '90125684', 'mira@mira11', '1231', '1231', '8100', '2018-03-01'),
(5, 212242, '1234(1).png', 'mmmm', 'mmmm', 'Homme', '79256314', 'lili.aloui@facebook.com', '111', 'tunis', 'o$02.3', '2018-03-12');

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE IF NOT EXISTS `avis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `produit` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `avis`
--

INSERT INTO `avis` (`id`, `email`, `produit`) VALUES
(3, 'amira@amira', 'sac120'),
(4, 'amira@amira', 'b120'),
(6, 'amira@amira', 'pent0012');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(100) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(100) NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `categorie`) VALUES
(1, 'Vetement'),
(2, 'Accessoire'),
(3, 'Chaussures'),
(4, 'Sacs'),
(6, 'Soins'),
(10, 'Parfums');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `cin_client` int(10) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `nom_client` varchar(100) NOT NULL,
  `prenom_client` varchar(100) NOT NULL,
  `genre` varchar(100) NOT NULL,
  `adresse_client` varchar(100) NOT NULL,
  `tel_client` varchar(100) NOT NULL,
  `email_client` varchar(100) NOT NULL,
  `pass_client` varchar(100) NOT NULL,
  `ville_client` varchar(100) NOT NULL,
  `poste` varchar(100) NOT NULL,
  `dtn` varchar(10) NOT NULL,
  PRIMARY KEY (`id_client`),
  UNIQUE KEY `cin_client` (`cin_client`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id_client`, `cin_client`, `photo`, `nom_client`, `prenom_client`, `genre`, `adresse_client`, `tel_client`, `email_client`, `pass_client`, `ville_client`, `poste`, `dtn`) VALUES
(2, 653421111, '12.jpeg', 'Amira', 'med', 'Femme', 'jendouba rue1546', '56235486', 'amira@amira', '456', 'jendouba', '8100', '2018-03-08'),
(3, 12345000, 'border.png', 'lmllm', 'mmmmlll', 'Homme', 'mmmm', '2222222', 'klk@kmlj', '456', 'tunis', '1200', '12/01/2010'),
(4, 112233, '001.jpg', 'mmflg', 'kjj', 'Femme', 'joij', '1215648', 'mmm@mm', 'mmm', 'mfhmg', '1202', '2018-03-08');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE IF NOT EXISTS `commande` (
  `id_commande` int(11) NOT NULL AUTO_INCREMENT,
  `email_client` varchar(100) NOT NULL,
  `ref_produit` varchar(100) NOT NULL,
  `quantite` int(11) NOT NULL,
  `date` varchar(10) NOT NULL,
  `heure` varchar(10) NOT NULL,
  `prix` varchar(100) NOT NULL,
  `etat` int(11) NOT NULL,
  PRIMARY KEY (`id_commande`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `commande`
--

INSERT INTO `commande` (`id_commande`, `email_client`, `ref_produit`, `quantite`, `date`, `heure`, `prix`, `etat`) VALUES
(1, 'amira@amira', 'vet123', 3, '28-05-2018', '00:00:26', '150', 1),
(2, 'amira@amira', 'vet123', 2, '28-05-2018', '00:09:25', '100', 1),
(3, 'amira@amira', 'vet123', 3, '28-05-2018', '00:11:53', '150', 1),
(4, 'amira@amira', 'chauss0', 5, '28-05-2018', '00:58:30', '1280', 2),
(5, 'amira@amira', 'cost1', 3, '28-05-2018', '13:54:03', '2160', 2),
(6, 'amira@amira', 'vet400', 1, '28-05-2018', '14:52:21', '59', 0);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE IF NOT EXISTS `commentaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `produit` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `date` varchar(10) NOT NULL,
  `commentaire` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `produit`, `email`, `date`, `commentaire`) VALUES
(1, 'vet133', 'amira@amira', '28-05-2018', 'lknln'),
(2, 'vet133', 'amira@amira', '28-05-2018', 'lknln'),
(3, 'vet133', 'amira@amira', '28-05-2018', 'lknln'),
(4, 'vet133', 'amira@amira', '28-05-2018', 'lknln'),
(5, 'vet133', 'amira@amira', '28-05-2018', 'lknln'),
(6, 'vet133', 'amira@amira', '28-05-2018', 'lknln'),
(7, 'vet133', 'amira@amira', '28-05-2018', 'lknln'),
(8, 'vet133', 'mmm@mm', '28-05-2018', 'migpiughbo'),
(9, 'vet133', 'mmm@mm', '28-05-2018', 'hhhhhh'),
(10, 'vet133', 'mmm@mm', '28-05-2018', 'hhhhhhhh'),
(11, 'vet133', 'mmm@mm', '28-05-2018', 'hhhhhhhh'),
(12, 'vet133', 'mmm@mm', '28-05-2018', 'hhhhhhhh'),
(13, 'vet133', 'mmm@mm', '28-05-2018', 'hhhhhhhh');

-- --------------------------------------------------------

--
-- Structure de la table `compte_clt`
--

CREATE TABLE IF NOT EXISTS `compte_clt` (
  `nom_carte` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `num_carte` varchar(100) NOT NULL,
  `code_secret` varchar(100) NOT NULL,
  `date_validite` varchar(100) NOT NULL,
  `solde` double NOT NULL,
  PRIMARY KEY (`num_carte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `compte_clt`
--

INSERT INTO `compte_clt` (`nom_carte`, `pass`, `num_carte`, `code_secret`, `date_validite`, `solde`) VALUES
('E-dinars', 'dad', '1234', '1234', '17/02/2019', 498024),
('Master', 'mery', '12345', '12345', '19/04/2020', 500000);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expediteur` varchar(100) NOT NULL,
  `distinataire` varchar(100) NOT NULL,
  `sujet` varchar(100) NOT NULL,
  `date` varchar(10) NOT NULL,
  `message` longtext NOT NULL,
  `notification` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`id`, `expediteur`, `distinataire`, `sujet`, `date`, `message`, `notification`) VALUES
(1, 'amira@amira', 'ala@live.com', 'salam', '12-04-2018', 'jhdpshgpi', 0),
(3, 'ala@live.com', 'admin@admin.fr', 'salam', '12/02/2018', 'jcbhmqk', 0),
(6, 'admin@admin.fr', 'amira@amira', '28-05-2018', 'salam', 'lfnmlsdnfl', 0),
(7, 'amira@amira', 'admin@admin.fr', '28-05-2018', 'Comment po', 'mmmmmm', 0);

-- --------------------------------------------------------

--
-- Structure de la table `moncompte`
--

CREATE TABLE IF NOT EXISTS `moncompte` (
  `num` int(11) NOT NULL,
  `solde` double NOT NULL,
  PRIMARY KEY (`num`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `moncompte`
--

INSERT INTO `moncompte` (`num`, `solde`) VALUES
(1, 501976);

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE IF NOT EXISTS `panier` (
  `id_panier` int(11) NOT NULL AUTO_INCREMENT,
  `email_client` varchar(100) NOT NULL,
  `ref_produit` varchar(100) NOT NULL,
  `quantite` int(11) NOT NULL,
  `date` varchar(100) NOT NULL,
  `heure` varchar(100) NOT NULL,
  `prix` varchar(100) NOT NULL,
  PRIMARY KEY (`id_panier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Contenu de la table `panier`
--

INSERT INTO `panier` (`id_panier`, `email_client`, `ref_produit`, `quantite`, `date`, `heure`, `prix`) VALUES
(21, 'amira@amira', 'vet400', 1, '28-05-2018', '14:52:21', '59');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_produit` varchar(100) NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `prix` varchar(100) NOT NULL,
  `date` varchar(10) NOT NULL,
  `quantite` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `marque` varchar(100) NOT NULL,
  `Genre` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `ref_produit`, `id_categorie`, `libelle`, `photo`, `prix`, `date`, `quantite`, `description`, `marque`, `Genre`) VALUES
(10, 'vet123', 1, 'Robe', 'p9.jpg', '250', '2018-03-30', 0, 'Robe de soirie', 'ZEN', 'Femme'),
(11, 'vet133', 1, 'Jupe avec Pull', 'si.jpg', '500', '2018-03-30', 100, 'jupe mini avec Pull ', 'Adara', 'Femme'),
(12, 'vet400', 1, 'pentalon', 'banner_img2.jpg', '59', '2018-03-31', 400, 'Pentalon  dans tous les couleurs', 'Aglini', 'Femme'),
(15, 'pul1230', 1, 'Pul', 'shirtt.jpg', '59', '2018-04-08', 120, 'Pul pour Homme ', 'ZEN', 'Homme'),
(16, 'sac120', 4, 'Sac a main', '001.jpg', '150', '2018-04-02', 100, 'Sac a main Rose Claire', 'Farmasi', 'Femme'),
(17, 'vest120', 1, 'Vest Jean', 'vest.jpg', '89', '2018-04-02', 150, 'Vest Jean pour femme', 'ZARA', 'Femme'),
(18, 'mont120', 2, 'Montre', '00110643318891____1__210x260.jpg', '350', '2018-04-03', 200, 'Montre Analog', 'Watches', 'Femme'),
(19, 'b120', 2, 'Bracelie', 'Personalize.jpg', '32', '2018-04-02', 125, 'Bracelie pour femme', 'Beclay', 'Femme'),
(20, 'Bij120', 2, 'Bijaux ', '3dlat.com_14163952995.jpeg', '500', '2018-04-02', 70, 'Bijaux', '3ADLT', 'Femme'),
(21, 'cost1', 1, 'Costume', 'cost.jpeg', '720', '2018-04-02', 100, 'Costume ', 'ZARA', 'Homme'),
(22, 'ch1', 1, 'Chemise', 'pntln.jpg', '56', '2018-04-03', 200, 'Chemise nouvelle collection', 'ZEN', 'Homme'),
(23, 'pent123', 1, 'Pentalon', 'vente.jpg', '110', '2018-04-03', 300, 'pentalon hommes', 'MONSTRA', 'Homme'),
(24, 'pent0012', 1, 'Pantalon', 'pent.jpg', '120', '2018-04-03', 100, 'YORTURN Pantalon Classique', 'YORTURN ', 'Homme'),
(25, 'pnt120', 1, 'Pantalon Sport', 'pantalon-sport.jpg', '50', '2018-04-03', 500, 'survÃªtement coton', 'LACOSTE', 'Homme'),
(26, 'pnt1200', 1, 'Pantalon', 'pntlnn.jpg', '85', '2018-04-03', 200, 'Vetement homme de marque STOENS\r\n', 'STOENS', 'Homme'),
(27, 'pntalon55', 1, 'Pantalon', 'pntlonnn.jpg', '90', '2018-04-03', 230, 'Pantalon TOP MARQUE pou femme', 'TOP MARQUE', 'Femme'),
(28, 'sac121', 4, 'Sac a main', 'pimkie1999-3.jpg', '120', '2018-04-02', 230, 'Sac a main cuir meilleur qualitÃ©', 'AFJ JEEP', 'Femme'),
(29, 'doux120', 4, 'Sac a dos', '3270.jpg', '75', '2018-04-02', 300, 'sac a dos femme', 'TU NAZA FASHION', 'Femme'),
(30, 'bcl01', 2, 'Boucle ', 'mlk.jpg', '19', '2018-04-02', 100, 'Boucle', 'Bijou Boucle', 'Femme'),
(31, 'parfum01', 10, 'Clm', 'ph3.jpg', '75', '2018-04-02', 20, 'Clm parfum pour les homme', 'Cristianlay', 'Homme'),
(32, 'parfum02', 10, 'New Age', 'ph2.jpg', '50', '2018-04-02', 50, 'New Age parfum pour les hommes', 'Cristianlay', 'Homme'),
(33, 'parfum03', 10, 'AQUA', 'pf4.jpg', '40', '2018-04-02', 30, 'AQUA parfum pour les femme ', 'Cristianlay', 'Femme'),
(34, 'parfum04', 10, 'Gold', 'pf9.jpg', '30', '2018-04-02', 35, 'Gold parfum pour les femmes', 'Cristianlay', 'Femme'),
(35, 'lun01', 2, 'Lunette', 'lunette.jpg', '200', '2018-04-03', 55, 'lunette soleil RAY BAN', 'RAY BAN', 'Homme'),
(36, 'lun02', 2, 'LUNETTE FEMME', '33334558fd0119bf30732c5d8e2a4cbc.jpg', '250', '2018-04-02', 56, 'Lunette RAY BAN pour les femmes', 'RAY BAN', 'Femme'),
(37, 'parfum05', 10, 'Parfum BEBE', 'bebe.jpg', '25', '2018-04-02', 34, 'Parfums pour les bebes', 'Cristianlay', 'Gamins'),
(38, 'vet01', 1, 'Combinaison bÃ©bÃ©', '01547854.jpg', '56', '2018-04-02', 31, 'Vetement pour les BÃ©BÃ©', 'TOP MARQUE', 'Gamins'),
(39, 'vest02', 1, 'VÃªtement bÃ©bÃ©', '1495.jpg', '120', '2018-04-02', 70, 'Vetement pour les bebe', 'ZEN', 'Gamins'),
(40, 'soin01', 6, 'Palette fard a popiere', 'palette.jpg', '99', '2018-04-02', 17, 'Palette fard a popiere pour les femmes', 'Cristianlay', 'Femme'),
(41, 'chauss01', 3, 'Chaussure', 'chaussureH.jpg', '150', '2018-04-03', 119, 'Chaussure pour Hommes', 'Cristianlay', 'Homme'),
(42, 'chauss02', 3, 'Chaussure sport', 'ChaussH.jpg', '56', '2018-04-02', 21, 'Chaussure sport pour hommes', 'ADIDAS', 'Homme'),
(43, 'chauss0', 3, 'Sandale Ã  talon ', 'ChaussF.jpg', '256', '2018-04-03', 150, 'Sandale a talon bon qualitÃ©', 'Cristianlay', 'Femme');

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

CREATE TABLE IF NOT EXISTS `promotion` (
  `id_promotion` int(11) NOT NULL AUTO_INCREMENT,
  `ref_produit` varchar(100) NOT NULL,
  `prix` varchar(100) NOT NULL,
  `date_debut_promo` varchar(10) NOT NULL,
  `date_fin_promo` varchar(10) NOT NULL,
  PRIMARY KEY (`id_promotion`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `promotion`
--

INSERT INTO `promotion` (`id_promotion`, `ref_produit`, `prix`, `date_debut_promo`, `date_fin_promo`) VALUES
(1, 'vet123 ', '50', '2018-04-06', '2018-04-22'),
(2, 'mont120 ', '90', '2018-04-06', '2018-04-19'),
(3, 'sac121 ', '25', '2018-04-18', '2018-04-28'),
(4, 'pul1230 ', '10', '2018-04-06', '2018-04-29'),
(5, 'pent123 ', '1000', '2018-12-02', '2018-12-31');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
