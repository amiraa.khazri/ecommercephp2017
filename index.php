<?php
session_start();

    include("admin/config/fonction.php");
    include("admin/admin/models/produit.php");
    include("admin/admin/models/categorie.php");
    include("admin/admin/models/promotion.php");
    include("admin/admin/models/agent.php");
    include("admin/admin/models/client.php");
    include("admin/admin/models/admin.php"); 
    include("admin/admin/models/panier.php");
    include("admin/admin/models/commande.php");
    include("admin/admin/models/contact.php");
    include("admin/admin/models/payer.php");
    include("admin/admin/models/avis.php");
    include("admin/admin/models/commentaire.php");

    connect();

	$date=date("d-m-Y");
    $heure=date("H:i:s");
    ?>

<!DOCTYPE html>
<html>


<head>
<title>Eshop**E-Commerce</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery.min.js"></script>
<!-- sweetAlrt -->
<script src="dist/sweetalert.min.js"></script>
<script rel="dist/stylesheet" type="text/css" href="sweetalert.css"></script>

<!-- Custom Theme files -->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />

<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Eshop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<!--webfont-->
<!-- for bootstrap working -->
	<script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>
<!-- //for bootstrap working -->
<!-- cart -->
	<script src="js/simpleCart.min.js"> </script>
<!-- cart -->
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
</head>
<body>
	<!-- header-section-starts -->
	<div class="header" style="margin-top:-20px;">
		<div class="header-top-strip">
			<div class="container">
				<div class="header-top-left">
					<ul>
                    
                    <?php
					if(!isset($_SESSION["email"]))
					{?>
						<li><a href="index.php?id=cnx#ctn"><span class="glyphicon glyphicon-user"> </span>Connexion</a></li>
						<li><a href="index.php?id=insc#ctn"><span class="glyphicon glyphicon-lock"> </span>Creer un Compte</a></li>	
                        
                    <?php
					}
					?>
					</ul>
				</div>
                
					<?php
                      if(isset($_SESSION["email"]))
                    {?>
                        <div class="header-top-left" >
                            <ul><li><a href="index.php?id=modif#ctn"><span class="glyphicon glyphicon-lock"> </span>Modifier Mon Compte</a></li>	</ul>
                        </div>
                
				<div class="header-right" style="margin-right:-100px!important">
                 <?php
                         $i = 0;
                         $clt = new client();
                         $clause ="where email_client='".$_SESSION['email']."'";
                         while ($i < compteurTable("client", $clause)) {
                          $clt->affiche_client($i, $clause)
                                                        
                    ?>
                                         
                      <p > <?php echo " <img style='border-radius: 15px;' src='admin/admin/photoproduit/$clt->photo' width=30 height=30>";?>
                      <?php echo "<b>". $clt->nom_client." ".$clt->prenom_client ." - ";  ?>
                      <a href="deconnect.php" style="color:#0CC"> Déconnecter </b></a> </p>
                             			
                    <?php
                       $i++;}
						
                                          
                     ?>
						
                            <div class="clearfix"> </div>
						</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
        
        		<div class="header-right" >
                
                   <div class="cart box_1" style="margin-right:50px!important">
                        <?php
							if(CompteurTable("panier","where email_client='".$_SESSION['email']."'")>0)
							{
						?>
								<a href="index.php?panier#ctn">
									<h3  style="color:#F9C !important"> <span ><?php echo Somme("panier","where email_client='".$_SESSION['email']."'") ; ?> Dt</span>
                                    <img src="images/bag.png" alt=""></h3>
								</a>	
								<p><a href="index.php?panier#ctn" class="simpleCart_empty"> <span id="simpleCart_quantity">
                            <?php echo CompteurTable("panier","where email_client='".$_SESSION["email"]."'"); ?></span> Article(s)</a></p>
								<?php
                                }
                                else if(CompteurTable("panier","where email_client='".$_SESSION['email']."'")==0)
                                {
                                ?>
								<p><a href="index.php?panier#ctn" class="simpleCart_empty"><img src="images/bag.png" alt=""> (0)</a></p>
								<?php
                                }
                            
                                }?>
				</div>
        
	</div>
	<!-- header-section-ends -->
			<div class="banner-top">
		<div class="container">
				<nav class="navbar navbar-default" role="navigation">
	    <div class="navbar-header">
	        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
	        </button>   
				<div class="logo">
					<h1><a href="index.php"><span>E</span> -Shop</a></h1>
				</div>
                </div>
	    <!--/.navbar-header-->
                      <!--recherche-->
                  <div>
                       <form method="post">
                            <input type="text" placeholder="Recherche par marque ..." name="rec" class="rech" required/><input type="submit" value="" name="rech"> 
                          
                        </form> 
                        <?php
						if(isset($_POST['rech']))
						{
							echo "<script>document.location.replace('index.php?rech=$_POST[rec]#ctn');</script>";
							
						}
						
						?>
                   </div> 
	    
        
              
	
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	        <ul class="nav navbar-nav">
			<li><a href="index.php?id=acc#ctn">	Accueil</a></li>
		        <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">HOMME <b class="caret"></b></a>
		            <ul class="dropdown-menu multi-column columns-2">
			            <div class="row">
				            <div class="col-sm-4">
					            <ul class="multi-column-dropdown">
                                <li><span class="x">CATÉGORIES</span></li>
                                <?php
								$i=0;
								$cat=new categorie();
								while($i<compteurTable("categorie",""))
								{ $cat->affiche_categorie($i,"");
								?>
                                	
						            <li><a href="index.php?cat=<?php echo $cat->id_categorie ; ?>&genre=Homme#ctn"><?php echo $cat->categorie ; ?></a></li>
                                <?php
								$i++;
								}
								?>
					            </ul>
				            </div>
							<div class="clearfix"></div>
			            </div>
		            </ul>
		        </li>
		        <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Femme <b class="caret"></b></a>
		            <ul class="dropdown-menu multi-column columns-2">
			            <div class="row">
				            <div class="col-sm-3">
					            <ul class="multi-column-dropdown">
                                 <li><span class="x">CATÉGORIES</span></li>
                                <?php
								$i=0;
								$cat=new categorie();
								while($i<compteurTable("categorie",""))
								{ $cat->affiche_categorie($i,"");
								?>
						            <li><a href="index.php?cat=<?php echo $cat->id_categorie ; ?>&genre=Femme#ctn"><?php echo $cat->categorie ; ?></a></li>
                                <?php
								$i++;
								}
								?>
					            </ul>
				            </div>
							<div class="clearfix"></div>
			            </div>
		            </ul>
		        </li>
		        <li class="dropdown">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Enfants <b class="caret"></b></a>
		            <ul class="dropdown-menu multi-column columns-2">
			            <div class="row">
				            <div class="col-sm-3">
					             <ul class="multi-column-dropdown">
                                  <li><span class="x">CATÉGORIES</span></li>
                                <?php
								$i=0;
								$cat=new categorie();
								while($i<compteurTable("categorie",""))
								{ $cat->affiche_categorie($i,"");
								?>
						            <li><a href="index.php?cat=<?php echo $cat->id_categorie ; ?>&genre=Gamins#ctn"><?php echo $cat->categorie ; ?></a></li>
                                <?php
								$i++;
								}
								?>
					            </ul>
				            </div>
							<div class="clearfix"></div>
			            </div>
		            </ul>
		        </li>
                    <?php
					if(isset($_SESSION["email"]))
					{
						?>
			    <li><a href="index.php?id=cmd#ctn">Mes commandes</a></li>
                <li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Messagerie <span style="background-color:#03F;color:white;font-weight:bold;border-radius:30px;box-shadow:1px 1px 1px gray;padding:2px 3px 2px 4px;top: -6px;right:-6px;font-size:1em;"><?php echo compteurTable("contact","where distinataire='".$_SESSION["email"]."' and notification='1'" ) ; ?></span> <b class="caret"></b></a>
		            <ul class="dropdown-menu multi-column columns-2">
			            <div class="row">
				            <div class="col-sm-4">
					            <ul class="multi-column-dropdown">
						            <li><a href="index.php?id=nouveau_msg#ctn">Nouveau</a></li>
						            <li><a href="index.php?id=env_msg#ctn">Envoyer</a></li>
						            <li><a href="index.php?id=recu_msg#ctn">Réçu</a></li>
					            </ul>
				            </div>
							<div class="clearfix"></div>
			            </div>
		            </ul>
		        </li>
                    <?php
					}
					?>
                    
	        </ul>
	    </div>
	    <!--/.navbar-collapse-->
        
        
	</nav>
	<!--/.navbar-->
    
    
     
</div>
</div>
  
             
        <?php if(!isset($_GET["panier"]))
		{?>

	<div class="banner">
		<div class="container">
<div class="banner-bottom">
	<div class="banner-bottom-left">
		<h2>V<br>E<br>N<br>T<br>E</h2>
	</div>
	<div class="banner-bottom-right">
   
		<div  class="callbacks_container">
       
					<ul class="rslides" id="slider4">
					<li>	<div class="banner-info">
									<h3>Vente en Ligne</h3>
									<p>Commencez vos achats ici...</p>
								</div>
							</li>
							<li>
								<div class="banner-info">
								   <h3>Magasin en ligne</h3>
									<p>Commencez vos achats ici...</p>
								</div>
							</li>
							<li>
								<div class="banner-info">
								  <h3>Prépare Ton valises</h3>
									<p>Commencez vos achats ic...</p>
								</div>								
							</li>
						</ul>
					</div>
					<!--banner-->
	  			<script src="js/responsiveslides.min.js"></script>
			 <script>
			    // You can also use "$(window).load(function() {"
			    $(function () {
			      // Slideshow 4
			      $("#slider4").responsiveSlides({
			        auto: true,
			        pager:true,
			        nav:false,
			        speed: 500,
			        namespace: "callbacks",
			        before: function () {
			          $('.events').append("<li>before event fired.</li>");
			        },
			        after: function () {
			          $('.events').append("<li>after event fired.</li>");
			        }
			      });
			
			    });
			  </script>
	</div>
	<div class="clearfix"> </div>
</div>
	</div>
		</div>
		<!---728x90--->
	<div class="container" >
	<!-- content-section-starts-here -->
			<div class="main-content" id="ctn">
				<div class="online-strip">
					<div class="col-md-4 follow-us">
						<h3>Suivez nous :</h3>
						<h3> <a class="twitter" href="#"></a><a class="facebook" href="#"></a></h3>
					</div>
					<div class="col-md-4 shipping-grid">
						<div class="shipping">
							<img src="images/shipping.png" alt="" />
						</div>
						<div class="shipping-text">
							<h3>Livraison gratuite</h3>
							<p>pour les commandes de plus de 199 dt</p>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-md-4 online-order">
						<p>Commander en ligne</p>
						<h3><span class="glyphicon  glyphicon-earphone"></span></span> Tel:99 567 892</h3>
					</div>
					<div class="clearfix"></div>
				</div>
                <?php 
		}
		?>
				<div class="products-grid">
                <?php
				if(isset($_GET["id"]))
				{
					if($_GET["id"]=="cnx") include ("vue/connection.php");
					if($_GET["id"]=="insc") include ("vue/inscription.php");
					if($_GET["id"]=="modif") include ("vue/modifer_compte.php");
					
					if($_GET["id"]=="acc") include ("vue/dernier_produit.php");
					if($_GET["id"]=="pay") include ("vue/payment.php");
					if($_GET["id"]=="recu") include ("vue/recu.php");
					
					if($_GET["id"]=="nouveau_msg") include ("vue/nouveau_msg.php");
					if($_GET["id"]=="env_msg") include ("vue/msg_envoyer.php");
					if($_GET["id"]=="recu_msg") include ("vue/msg_recu.php");
					if($_GET["id"]=="cmd") include ("vue/commande.php");
					
									
					
				}
				else if(isset($_GET["cat"]))
				{
					$genre=$_GET["genre"];
					include("vue/produit.php");
					
				}
				else if(isset($_GET["detail"]))
				{
					include("vue/detail_produit.php");
					
					
				}
				else if(isset($_GET["panier"]))
				{
				   include("vue/panier.php");	
				}
				else if(isset($_GET["supp_panier"]))
				{
					$p = new panier();
					$p -> supprimer_panier($_GET["supp_panier"]);
					
					$p = new commande();
					$p -> supprimer_commande($_GET["supp_panier"]);
					
				}
				else if(isset($_GET["supp_contact_env"]))
				{
					$c = new contact();
					$c -> supprimer_contact_env($_GET["supp_contact_env"]);
				}
				else if(isset($_GET["supp_contact_rec"]))
				{
					$c = new contact();
					$c -> supprimer_contact_rec($_GET["supp_contact_rec"]);
				}
				else if(isset($_GET["rech"]))
				{
					include("vue/recherche.php");
				}
				else
				{
					include ("vue/dernier_produit.php");
				}
				?>







				</div>
			</div>

		</div>
		<div class="other-products">
		<div class="container">
			<h3 class="like text-center">Nos  Produits</h3> 
          	 <ul id="flexiselDemo3">
  
            <?php
				  $i=0;
				  $cat = new produit();
				  $clause="";
				  while($i<CompteurTable("produit",$clause))
				  {
					  $cat ->affiche_produit($i,$clause);
			?>
                   			
						<li>
                        <a href="index.php"><img src="admin/admin/photoproduit/<?php echo $cat->photo; ?>" alt="" width="100" height="80"  /></a>
							<div class="product liked-product simpleCart_shelfItem">
							<a class="like_name" href="#"><?php echo $cat->libelle; ?></a>
							<p><a class="item_add" href="#"><i></i> <span class=" item_price"><?php echo $cat->prix; ?>Dt</span></a></p>
							</div>
						</li>
                       <?php
					$i++;
				  }
				  ?>
			 </ul>
                 <script type="text/javascript">
					 $(window).load(function() {
						$("#flexiselDemo3").flexisel({
							visibleItems: 4,
							animationSpeed: 1000,
							autoPlay: true,
							autoPlaySpeed: 3000,    		
							pauseOnHover: true,
							enableResponsiveBreakpoints: true,
					    	responsiveBreakpoints: { 
					    		portrait: { 
					    			changePoint:480,
					    			visibleItems: 1
					    		}, 
					    		landscape: { 
					    			changePoint:640,
					    			visibleItems: 2
					    		},
					    		tablet: { 
					    			changePoint:768,
					    			visibleItems: 3
					    		}
					    	}
					    });
					    
					});
				   </script>
				   <script type="text/javascript" src="js/jquery.flexisel.js"></script>
				   </div>
				   </div>
		<div class="footer">
		<div class="container">
		  <div class="cards text-center">
				<img src="images/cards.jpg" alt="" />
		  </div>
          
          <div class="cards text-center">
			<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1on-d6JHzCfOdLwxuvAeSND1mcGdvAfAk" width="100%" height="400"></iframe>
		  </div>
          
		  <div class="copyright text-center">
				<p>© 2018 E-shop. Tous les droits sont réservés |    <a href="admin/" >Amira khazri </a></p>
		  </div>
		</div>
		</div>
</body>

</html>